#!/usr/bin/env node
// SMS login hashes the phone # with a secret given in secrets.js
// I needed a way to know what the hash turns out to be to add people to ACLs
// Thus, this helper

'use strict';
const bcrypt = require('bcrypt'), obj = require('hex-object');
if (process.argv[2] === 'salt' && /^\d+$/.test(process.argv[3])) {
	bcrypt.genSalt(parseInt(process.argv[3]), (err, res) => {
		if (err) { throw err; };
		console.log(res);
	});
}
else if (process.argv[2] === 'hash' && process.argv[3]) {
	const num = process.argv[3].replace(/\D/g, ''), secrets = obj.wrap(require(`${process.cwd()}/secrets`));
	secrets.normalize();
	bcrypt.hash(num, secrets.get('passport.strategies.sms.salt'), (err, hash) => {
		if (err) throw err;
		console.log(hash);
	});
}
else {
	console.log(
`Usage: ${require('path').basename(process.argv[1])} salt <rounds>, or hash <phone>
hash variant should be run in the same directory as secrets.js defining passport.strategies.sms.salt.
Remember the country code for the phone number. Non-digit characters are stripped.`
	);
	process.exit(1);
}
