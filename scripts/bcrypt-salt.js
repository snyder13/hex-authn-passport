/**
 * Usage: node bcrypt-salt <rounds, default 15>
 *
 * Required for hashing for stored phone #s in the SMS plugin, in secrets.js 'passport.strategies.sms.salt'
 */
'use strict';
const bcrypt = require('bcrypt');
console.log(bcrypt.genSaltSync(process.argv[2] ? process.argv[2] : 15));
