'use strict';
const fs = require('fs');
const redis = require('redis');
const bluebird = require('bluebird');
const obj = require('hex-object');
const prmpt = require('prompt');
const cwd = process.cwd();
const conf = obj.wrap(require(`${ cwd }/conf.js`));
const rounds = conf.get('bcrypt.rounds', 15);
const bcrypt = require('bcrypt');
if (fs.existsSync(`${ cwd }/secrets.js`)) {
	conf.augment(require(`${ cwd }/secrets.js`));
}

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const dbh = redis.createClient(conf.get('redis', {}));

prmpt.start();
bluebird.promisify(prmpt.get)({
	'properties': {
		'domain': {
			'default': 'local'
		},
		'username': {},
		'password': {
			'hidden': true
		},
		'props': {
			'description': 'additional properties? (JSON {"key": "value", ...})',
			'default': '{}'
		}
	}
})
	.then(({ domain, username, password, props }) => {
		props = JSON.parse(props);
		props.hash = bcrypt.hashSync(username, rounds);
		props.username = username;
		const tbl = `hex:user:${ username }@${ domain }`;
		return dbh.multi()
			.del(tbl)
			.hmset(tbl, [].concat(...Object.entries(props)))
			.persist(tbl)
			.execAsync();
	})
	.then(() => dbh.end(true));

