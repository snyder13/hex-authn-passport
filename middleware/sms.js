'use strict';
// @flow

const bcrypt = require('bcrypt');
const custom = require('passport-custom');
const Twilio = require('twilio');

import type { Context } from 'express-hex/types/hex';

/**
 * Log in with SMS. Not really advised, but could be adapted into part of a two-factor, which I understand is also not advised.
 */
module.exports = ({ app }: Context) => {
	const { passport, router, conf, linkBase } = app.registerPassportStrategy('sms');

	const reqd = {
		'salt': 'bcrypt salt to generate ids from phone numbers',
		'from': 'Twilio phone # to act as sender',
		'client.id' : 'Twilio account id',
		'client.secret': 'Twilio account authorization'
	}, missing = [];
	Object.keys(reqd).forEach((k) => {
		if (/^client/.test(k)) {
			if (!conf.client[k.replace(/^client[.]/, '')]) {
				missing.push(`${ k }: ${ reqd[k] }`);
			}
		}
		else {
			if (!conf[k]) {
				missing.push(`${ k }: ${ reqd[k] }`);
			}
		}
	});
	if (missing.length) {
		throw new Error(`missing configuration parameters for sms auth: ${ missing.join(', ') }`);
	}


	const codeMsg = conf['code-message'] ? conf['code-message'] : 'Hello! Enter code {{code}} to sign in. If you don\'t know what this is about, please disregard. Reply "stop" to block your number on this service.';
	if (!/\{\{code\}\}/.test(codeMsg)) {
		throw new Error(`No slot for the code is defined in the SMS code message. Use {{code}} somewhere: ${ codeMsg }`);
	}

	// try to set up with supplied twilio params
	const twilio = new Twilio(conf.client.id, conf.client.secret);
	const max = {
		'tries': conf['max-tries'] ? conf['max-tries'] : 5,
		'lifetime': conf['expiry-seconds'] ? conf['expiry-seconds'] : 10 * 60 * 1000
	};

	passport.use('sms', new custom((req, done) => {
		// authn can't possibly proceed b/c of missing or expired data, or too many attempts
		if (!req.session.sms || req.session.sms.expires < (new Date).getTime() || --req.session.sms.tries <= 0) {
			// resetting the session will send them back to the phone entry page rather than code entry
			// (and prevent further attempts at the same code)
			req.session.sms = {};
			return done(null, null, { 'message': 'Invalid request. Try getting a fresh code.' });
		}
		// improper format
		if (!req.body.code || !/^\d{5}$/.test(req.body.code)) {
			return done(null, null, { 'message': 'Invalid code, please enter the five-digit code sent to your phone.' });
		}
		// good format but doesn't match
		if (req.body.code !== req.session.sms.code) {
			return done(null, null, { 'message': 'That\'s not the one. Typo?' });
		}

		// matches, send opaque id generated from phone #
		// @TODO be nice to look this up and send more usable session info like an associated username
		bcrypt.hash(req.session.sms.phone, conf.salt, (err, id) => {
			delete req.session.sms;
			done(null, { id, 'domain': 'sms' });
		})
	}));

	// phone # entry form
	router.get('/sms', app.csrf, (req, res) => {
		res.render('passport/sms', { 'message': getMessage(req), 'csrf': req.csrfToken() });
	});

	// phone # posted, send a code to it
	router.post('/sms/send', (req, res) => {
		if (!req.session.sms) {
			req.session.sms = {};
		}
		if (!req.body.country || !req.body.phone) {
			req.session.sms.message = 'Invalid phone number';
			return res.redirect(`${linkBase}/sms`);
		}
		const phone = (req.body.country + req.body.phone).replace(/\D+/g, '');
		if (!/^\d{11,13}$/.test(phone)) {
			req.session.sms.message = 'Invalid phone number';
			return res.redirect(`${linkBase}/sms`);
		}
		req.session.sms = {
			phone,
			'tries': max.tries,
			'code': genCode(),
			'expires': (new Date).getTime() + max.lifetime
		};
		twilio.messages.create({
			'to': phone,
			'from': conf.from,
			'body': codeMsg.replace('{{code}}', req.session.sms.code)
		});
		res.redirect(`${linkBase}/sms/code`);
	});

	// code entry form
	router.get('/sms/code', app.csrf, (req, res) => {
		if (!req.session.sms || !req.session.sms.code) {
			return res.redirect(`${linkBase}/sms`);
		}
		res.render('passport/sms-code', { 'message': getMessage(req), 'csrf': req.csrfToken() });
	});

	// code posted, see if it's acceptable
	router.post('/sms/confirm',
		passport.authenticate('sms', { 'failureRedirect': `${linkBase}/sms/code`, 'failureFlash': true }),
		app.handleLogin
	);

};

function genCode() {
	const zeroPad = '00000', code = Math.floor(Math.random() * (Math.pow(10, zeroPad.length) - 1));
	return zeroPad.substr(0, zeroPad.length - code.toString().length) + code.toString();
}

function getMessage(req) {
	let message = null;
	if (req.session.sms && req.session.sms.message) {
		message = req.session.sms.message;
		delete req.session.sms.message;
	}
	return message || req.flash('error');
}
