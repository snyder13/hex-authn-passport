'use strict';
// @flow

const jwt = require('jsonwebtoken');
const dur = require('dur');
const expiry = '60s';
const uuid = require('uuid/v4');

import type { Context } from 'express-hex/types/hex';

/**
 * Used to log using another service that is running this code, according to whatever methods it has enabled
 */
module.exports = (ctx: Context) => {
	const { app, log } = ctx;
	let fwdPort = ctx.conf.get('http.port');
	fwdPort = fwdPort[0] == 80 || fwdPort[0] == 443 || fwdPort.length > 1 ? '' : `:${ fwdPort[0] }`;

	const { linkBase, proxyBase, conf, router } = app.registerPassportStrategy('hex');

	// you must define in conf.js where the login server resides, and do a key enchange with it
	if (!conf.service || !conf.service.url || !conf.service['server-public'] || !conf.service['client-private']) {
		console.error('no service/key pair defined for hex login');
		process.exit(1);
	}

	app.use((req, res, next) => {
		console.log(req.path);
		next();
	});

	let expiresIn = conf.expiry ? conf.expiry : expiry;
	// redirecting a user to /login/hex (assuming /login is the base path for these middleware) in turn
	// redirects them to the actual login service with a JWT describing how to return their session
	// information to us
	router.get('/hex', (req, res) => {
		if (req.query.return) {
			req.session.return = Buffer.from(req.query.return, 'base64').toString();
		}
		const token = jwt.sign({
			'callback': `http${req.secure ? 's' : ''}://${ req.hostname }${ fwdPort }${ linkBase }/hex/callback`,
			'expiry': dur(expiresIn)/1000
		}, conf.service['client-private'], { expiresIn, 'algorithm': 'RS256', 'jwtid': uuid() })

		// provide hostname in addition to token so service knows which key to use to verify it
		res.redirect(`${ conf.service.url }?return=${ token }&host=${ req.hostname }${ fwdPort }`);
	});

	// user returns from login service
	router.get('/hex/callback', (req, res) => {
		if (!req.query.jwt) {
			return res.status(400).send();
		}

		jwt.verify(req.query.jwt, conf.service['server-public'], { 'algorithms': [ 'RS256' ] }, (err, user) => {
			if (err) {
				log.error('jwt error', err);
				return res.status(403).send('Forbidden');
			}

			app.redis.getAsync(`hex:jwt:${user.jti}`)
				.then((tok) => {
					// no double spends
					if (tok) {
						return res.status(403).send('Forbidden');
					}
					return app.redis.multi()
						.set(`hex:jwt:${user.jti}`, 1)
						.expire(`hex:jwt:${user.jti}`, user.expiry)
						.execAsync()
							.then(() => user);
				})
				.then((user) => {
					[ 'expiry', 'iat', 'exp', 'jti' ].forEach((k) => {
						if (user[k]) {
							delete user[k];
						}
					});

					log.debug('login', { user });
					req.login(user, (err) => {
						if (err) {
							log.error(err);
							return res.status(500).send('Internal Server Error');
						}

						// redirect where they came from if a local URL was stored, or just to the main page
						let ret = proxyBase === '' ? '/' : proxyBase;
						if (req.session.return) {
							ret = req.session.return;
							delete req.session.return;
						}
						res.redirect(/^\//.test(ret) ? ret : '/');
					});
				});
		});
	});
}
