'use strict';
// @flow

import type { Context } from 'express-hex/types/hex';

const { URL } = require('url');
const uuid = require('uuid/v4');
const jwt = require('jsonwebtoken');
const dur = require('dur');
const expiry = '60s';

/**
 * Use this middleware to operate in "pure" mode, only providing logins for other services that
 * redirect here through the 'hex' plugin. This service is assumed not to do anything else with its time.
 */
module.exports = ({ app, conf, log }: Context) => {
	// domains need to do RS256 key exchange in order to have their login requests forwarded back
	const fwdDomains = conf.get('passport.standalone.forward-domains', {});
	const pkey = conf.get('passport.standalone.key');

	if (Object.keys(fwdDomains).length == 0) {
		log.error('no forward domains defined, can\'t provide authn in standalone mode');
		process.exit(1);
	}

	app.use((req, res, next) => {
		// when the query string looks like a redirected login attempt, and we know the host
		if (req.query.return && req.query.host && fwdDomains[req.query.host]) {
			// verify origin
			jwt.verify(req.query.return, fwdDomains[req.query.host], { 'algorithms': [ 'RS256' ] }, (err, ret) => {
				if (err) {
					log.error(err);
					return cont();
				}
				if (ret.callback) {
					const url = new URL(ret.callback);
					if (url.host === req.query.host) {
						req.session['export-session'] = ret.callback;
						req.session['export-host'] = url.host;
						// get rid of base64 junk in url
						return res.redirect(req.path);
					}
				}
				return cont();
			});
		}
		else {
			cont();
		}
		function cont() {
			// if no callback was validated, block every request
			if (!req.session['export-session']) {
				return res.status(403).send('Forbidden');
			}
			// otherwise, let the other plugins operate as they normally do
			next();
		}
	});

	if (!app.loginHandlers) {
		app.loginHandlers = [];
	}
	app.loginHandlers.unshift((req, res, next) => {
		// shouldn't normally be reachable, just in case
		if (!req.session || !req.session['export-session'] || !req.session['export-host']) {
			log.debug('share-session: no session or no return', { 'session': req.session });
			return next();
		}
		// incoming jwt previously verified, send back session info
		log.debug('share-session: found forward domain', req.session['export-session']);
		const token = jwt.sign(Object.assign(JSON.parse(req.session.passport.user), { 'expiry': dur(expiry) / 1000 }), pkey, { 'algorithm': 'RS256', 'expiresIn': expiry, 'jwtid': uuid() });
		res.redirect(`${req.session['export-session']}${req.session['export-session'].indexOf('?') > -1 ? '&' : '?'}jwt=${token}`);
	});
};
