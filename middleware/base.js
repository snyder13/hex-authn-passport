'use strict';
// @flow

const passport = require('passport');

import type { Context } from 'express-hex/types/hex';

/**
 * Set up behaviors that are common among login providers
 */
module.exports = ({ app, express, conf, log }: Context) => {
	// helper that persists query string ?return=/foo
	app.use((req, res, next) => {
		if (req.query.return) {
			req.session.loginReturn = req.query.return;
		}
		next();
	});

	// users are JSON in the session store
	passport.serializeUser((user, done) => done(null, JSON.stringify(user)))
	passport.deserializeUser((user, done) => done(null, JSON.parse(user)));

	app.use(passport.initialize());
	app.use(passport.session());
	// usable in certain template formats to show error messages
	app.use(require('connect-flash')());

	// used when a front proxy installs the application in a given path
	// eg, everything to /session gets routed here, proxyBase is /session
	const proxyBase = conf.get('http.proxyBase', '');
	// install routes under this name. arbitrary
	const loginBase = conf.get('passport.base', '/login').replace(/\/$/, '');
	const linkBase = proxyBase + loginBase;

	// store link base href for plugins, which need to route things with this prefix
	app.use((req, res, next) => {
		res.locals.linkBase = linkBase;
		next();
	});

	log.info('passport paths', { loginBase, linkBase });

	const router = express.Router();
	app.use(loginBase, router);

	// save list of links to enabled strategies for base page
	const links = [];
	const stratConf = conf.get('passport.strategies', {});

	// called by plugins to register themselves to list in the main /login page, and to get the context they need to implement their provider
	app.registerPassportStrategy = (strat, link) => {
		if (!stratConf[strat]) {
			stratConf[strat] = {};
		}
		// build link from main /login page, if one wasn't already suggested
		// takes a label from conf.js or just mangles one out of the plugin name
		if (!link) {
			link = `<a href="${linkBase}/${strat}">${stratConf[strat].label ? stratConf[strat].label : strat.charAt(0).toUpperCase() + strat.slice(1) + ' account'}</a>`;
		}
		links.push({ strat, link });
		return { passport, router, linkBase, proxyBase, 'conf': stratConf[strat] };
	};

	// maintain a stack of callbacks that happen when a new log in occurs
	if (!app.loginHandlers) {
		app.loginHandlers = [];
	}
	// default returns user to wherever they came from that asked them to log in, presuming that info is available
	// exporting login information through a JWT instead is also done with a login handler, elsewhere
	app.loginHandlers.push((req, res, next) => {
		let ret = proxyBase;
		if (req.session.loginReturn) {
			ret = req.session.loginReturn;
			delete req.session.loginReturn;
		}
		// only redirect locally by default
		res.redirect((/^\//).test(ret) ? ret : '/');
	});
	// call all login handlers. they can short-circuit by not calling their next() param
	app.handleLogin = (req, res) => {
		let cbIdx = -1;
		const next = () => {
			if (++cbIdx === app.loginHandlers.length) {
				return;
			}
			app.loginHandlers[cbIdx](req, res, next);
		};
		next();
	}

	// main /login page, with list of links to methods
	router.get('/', (req, res, next) => {
		// ... except if there's only one enabled, then just go there
		if (links.length === 1) {
			return res.redirect(`${loginBase}/${links[0].strat}`);
		}
		res.render('passport/login', { links });
	});

	// use passport's built-in logout function
	router.get('/destroy', (req, res, next) => {
		req.logout();
		res.redirect(proxyBase === '' ? '/' : proxyBase);
	});
};
