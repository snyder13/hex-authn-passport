'use strict';
module.exports = {
	'base': {
		'description': 'Passport initialization for authn strategies',
		'deps': [ 'hex-session.session', 'hex-views-handlebars.engine' ],
		'after': [ 'standalone' ]
	},
	'standalone': {
		'description': 'Operate only as a login service, forwarding sessions to other apps',
		'deps': [ 'hex-session.session' ]
	},
	'local': {
		'description': 'Local login with Redis, mostly proof-of-concept',
		'deps': [ 'base', 'hex-extras.redis', 'hex-extras.csrf' ]
	},
	'shibboleth': {
		'description': 'Shibboleth/InCommon login (stub w/ testshib identity provider only)',
		'deps': [ 'base' ]
	},
	'sms': {
		'description': 'Log in with a SMS code. Requires Twilio API key',
		'deps': [ 'base', 'hex-extras.csrf', 'hex-extras.redis' ]
	},
	'google': {
		'description': 'Google G+ login',
		'deps': [ 'base' ]
	},
	'hex': {
		'description': 'Log in with another hex server that implements some Passport strategies, using JSON web tokens to transport and verify session (requires key exchange between these two services)',
		'deps': [ 'base', 'hex-extras.redis' ]
	}
};
