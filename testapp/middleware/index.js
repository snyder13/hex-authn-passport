'use strict';
module.exports = ({ app }) => {
	app.get('/', (req, res) => {
		res.type('html').send(`<!doctype html>
<html>
	<body>
		<pre>${ JSON.stringify(req.session, null, '\t') }</pre>
		<a href="/login">Log in</a>
	</body>
</html>`);
	});
}
