const fs = require('fs');
module.exports = {
	'index': {
		'description': 'Main page',
		// consumer only loads the hex plugin, standalone loads all the other strategies
		'deps': process.env.NODE_PORT == 4000 ? [ 'hex-authn-passport.hex' ] : fs.readdirSync('../middleware')
			.filter((file) => /[.]js$/.test(file) && file !== 'hex.js')
			.map((file) => `hex-authn-passport.${ file.replace(/[.]js$/, '') }`)
	}
};
