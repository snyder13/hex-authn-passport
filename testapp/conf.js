'use strict';
const fs = require('fs');

module.exports = {
	'log.auth': `${ __dirname }/auth.log`,
	'session.secret': 'not very secret',
	'passport.strategies.hex.service': {
		'url': 'http://localhost:4001/login',
		'server-public': fs.readFileSync('keys/standalone.pub', { 'encoding': 'utf8' }),
		'client-private': fs.readFileSync('keys/consumer', { 'encoding': 'utf8' })
	},
	'passport.strategies.google': {
		'callback': 'https://localhost:4000/login/google/callback',
		'client': {
			'id': '$API_ID',
			'secret': '$API_KEY'
		}
	},
	'passport.strategies.sms': {
		'from': '18005551212',
		'salt': 'script/bcrypt-salt to generate one',
		'client': {
			'id': 'AC$API_ID',
			'secret': '$API_KEY'
		}
	},
	'passport.standalone': {
		'key': fs.readFileSync('keys/standalone', { 'encoding': 'utf8' }),
		'forward-domains': {
			'localhost:4000': fs.readFileSync('keys/consumer.pub', { 'encoding': 'utf8' })
		}
	}
};
