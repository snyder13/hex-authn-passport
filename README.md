# hex-authn-passport
Middleware implementating various login strategies. Written hastily, but used regularly (Twilio SMS plugin, anyway).

See [express-hex](https://bitbucket.org/snyder13/hex) for a general explanation of how this type of middleware works.

A running Redis server is currently required for token expiry bookkeeping and for account storage with the `local` plugin.


## Provided middleware
 * `base` - Basic behaviors including Passport initialization, a `/login` page listing available plugins, log-out, session serialization. Not necessary to include directly, pulled in by all plugins
 * `standalone` - There are two modes of operation: as the `/login` component of an application containing other stuff, or include this for a standalone mode that *only* does authentication, and communicates back sessions to other services via JSON web tokens. See `middleware/standlone.js` for more info.
 * plugins:
 	* `local` - Authenticate against a Redis database. (Could be trivially changed to other stores)
	* `shibboleth` - Barely-working example of how Shibboleth could work, only currently works with TestShib. Should actually be done through the SAML plugin.
	* `sms` - Log in with an SMS code, caveat emptor
	* `google` - Log in with Google. See `middleware/google.js` for info about how to set up your Google API account
	* `hex` - Log in by redirecting to another server that is running this library with the `standalone` mode engaged. This shows what other software would have to do to interface with a standalone server running independently. Namely, do key exchange, sign a token with the URL you would like the session JWT returned to, redirect, and listen for that callback. The callback should then attempt to prevent double spends and token expiry.

## Test app
Run `yarn testapp` to bootstrap the test application, then in the `testapp` path run:

`DEBUG=hex:* node consumer-server`
and, in other terminal or having sent that to the background:
`DEUB=hex:* node standalone-server`

In this setup, the consumer server on :4000 will redirect attempts at `/login` to the standalone server on :4001.

`http://localhost:4000/` will print your session information.

The standalone server has all the plugins enabled, though most require API keys to be added to the configuration.

`cd testapp && node ../scripts/add-redis-user` will let you put in a test account that will work for the purposes of the `local` plugin.
